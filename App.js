import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
const App = () => {
  const [kategori, setKategori] = useState([
    {
      nama: 'Foto',
    },
    {
      nama: 'Undangan',
    },
    {
      nama: 'Cetak Foto',
    },
    {
      nama: 'Print File',
    },
    {
      nama: 'Tatarias',
    },
    {
      nama: 'Dekor',
    },
    {
      nama: 'Foto Hunting',
    },
  ]);

  const [kategoriSeleksi, setKategoriSeleksi] = useState({
    nama: 'Ayam',
  });

  const [dataTrending, setDataTrending] = useState([
    {
      namaResep: 'Foto Wedding Paket Standart',
      author: 'Rp.350.000',
      image: require('./src/images/1.jpg'),
    },
    {
      namaResep: 'Foto Wedding Paket Turbo',
      author: 'Rp.500.000',
      image: require('./src/images/2.jpg'),
    },
    {
      namaResep: 'Foto Wedding Paket Unlimitid',
      author: 'Rp.2.500.000',
      image: require('./src/images/3.jpg'),
    },
    {
      namaResep: 'Foto Hunting',
      author: 'RP.5.000',
      image: require('./src/images/4.jpg'),
    },
  ]);

  const [dataVideo, setDataVideo] = useState([
    {
      namaResep: 'Foto Wedding Paket Standart',
      author: 'Rp.350.000',
      image: require('./src/images/1.jpg'),
      length: '10:10',
    },
    {
      namaResep: 'Foto Wedding Paket Turbo',
      author: 'Rp.500.000',
      image: require('./src/images/2.jpg'),
      length: '09:09',
    },
    {
      namaResep: 'Foto Wedding Paket Unlimitid',
      author: 'Rp.2.500.000',
      image: require('./src/images/3.jpg'),
      length: '12:13',
    },
    {
      namaResep: 'Foto Hunting',
      author: 'RP.5.000',
      image: require('./src/images/4.jpg'),
      length: '12:14',
    },
  ]);

  return (
    <View style={{flex: 1, backgroundColor: '#f5f5f5'}}>
      <ScrollView>
        <StatusBar backgroundColor="#f5f5f5" barStyle="dark-content" />
        <View style={{marginHorizontal: 20, marginBottom: 20, marginTop: 20}}>
          <Text style={{fontSize: 28, fontWeight: 'bold', color: '#212121'}}>
            Ady Studio<Text style={{color: '#4169e1'}}>Foto</Text>
          </Text>
        </View>
        <View>
          <FlatList
            data={kategori}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor:
                    kategoriSeleksi.nama == item.nama ? '#4169e1' : '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}
                onPress={() => setKategoriSeleksi(item)}>
                <Text
                  style={{
                    color:
                      kategoriSeleksi.nama == item.nama ? '#fff' : '#212121',
                  }}>
                  {item.nama}
                </Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#212121'}}>
              Trending
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataTrending}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <Image
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}
                />
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>

        <View
          style={{
            marginHorizontal: 20,
            marginBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#212121'}}>
              Video Foto
            </Text>
          </View>

          <TouchableOpacity
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'row',
              marginTop: 10,
            }}>
            <Text style={{fontSize: 14}}>Lihat Semua</Text>
            <Icon name="chevron-forward" size={20} color="#bdbdbd" />
          </TouchableOpacity>
        </View>

        <View>
          <FlatList
            data={dataVideo}
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{marginLeft: 10}}
            renderItem={({item}) => (
              <TouchableOpacity
                style={{
                  marginRight: 5,
                  backgroundColor: '#fff',
                  elevation: 3,
                  paddingHorizontal: 15,
                  paddingVertical: 8,
                  marginBottom: 10,
                  borderRadius: 15,
                  marginLeft: 5,
                }}>
                <ImageBackground
                  source={item.image}
                  style={{
                    width: 200,
                    height: 150,
                    marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 3,
                  }}
                  resizeMode={'stretch'}>
                  <View style={{flex: 1}}>
                    <View style={{flex: 1}}></View>
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingTop: 2,
                          paddingBottom: 2,
                        }}>
                        <Icon
                          style={{marginLeft: 5}}
                          name="play-circle"
                          size={15}
                          color="#bdbdbd"
                        />
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: 'rgba(0,0,0,0.7)',
                          paddingRight: 10,
                          paddingTop: 2,
                          paddingBottom: 2,
                          paddingLeft: 4,
                        }}>
                        <Text style={{color: '#FFFFFF'}}>{item.length}</Text>
                      </View>
                    </View>
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    color: '#212121',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  {item.namaResep}
                </Text>
                <Text>{item.author}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          paddingTop: 5,
          backgroundColor: '#FFFFFF',
        }}>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="home" size={25} color="#4169e1" />
          <Text style={{color: '#4169e1'}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="search" size={25} color="#bdbdbd" />
          <Text style={{color: '#bdbdbd'}}>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="file-tray-full" size={25} color="#bdbdbd" />
          <Text style={{color: '#bdbdbd'}}>Kategori</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
          <Icon name="person" size={25} color="#bdbdbd" />
          <Text style={{color: '#bdbdbd'}}>User</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;
